import { writable } from 'svelte/store'

export const activeFile = writable({
    type: '',
    name: '',
    label: ''
})