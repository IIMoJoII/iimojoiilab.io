import { writable } from 'svelte/store'

export const createFileModal = writable({
    show: false,
    label: '',
    action: ''
})