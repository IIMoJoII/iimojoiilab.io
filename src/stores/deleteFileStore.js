import { writable } from 'svelte/store'

export const deleteFileModal = writable({
    show: false,
    type: '',
    label: ''
})