export const setItem = ({
  key = '',
  value = ''
} = {}) => {
  try {
    localStorage.setItem(key, value)
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
  }
}

export const getItem = ({
  key = ''
} = {}) => {
  try {
    return localStorage.getItem(key)
  }
  catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
  }
}

export const removeItem = ({
  key = ''
} = {}) => {
  try {
    localStorage.removeItem(key)
  }
  catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
  }
}