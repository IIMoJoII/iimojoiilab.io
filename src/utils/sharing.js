import { constructQueryString } from './queryUtils'

export const getSharingLinkVK = ({
  url,
  title,
  description,
  image
}) => `http://vk.com/share.php${constructQueryString({
  url,
  title,
  description,
  image
})}`

export const getSharingLinkFB = ({
  url
}) => `http://www.facebook.com/sharer.php${constructQueryString({
  u: url
})}`

export const getSharingLinkOK = ({
  url,
  title,
  description,
  image
}) => `https://connect.ok.ru/offer${constructQueryString({
  url,
  title,
  description,
  image
})}`