import { donutExample, inputExample, maskedInputExample, sharingExample, formExample, selectExample } from '../exapmples'

let componentsByPage = [
    { path: '/MaskedInput', component: maskedInputExample },
    { path: '/Donut', component: donutExample },
    { path: '/Sharing', component: sharingExample },
    { path: '/Form', component: formExample },
    { path: '/Input', component: inputExample },
    { path: '/Select', component: selectExample }
]

export const getComponent = (path) => {
    let currentComponent = []
    componentsByPage.map(component =>
        component.path === path && (currentComponent = component.component)
    )

    return currentComponent
}