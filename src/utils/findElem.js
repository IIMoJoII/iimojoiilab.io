export const findElem = (queryString, root = null) => {
  const elem = (root || document).querySelector(queryString);
  if (!elem) throw new Error(`No such element: ${queryString}`);
  return elem;
}