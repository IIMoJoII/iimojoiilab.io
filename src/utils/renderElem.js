/** Утилити функция для создания HTML элементов */
export const renderElem = ({
  name = 'div',
  className = null,
  style = null,
  attributes = null,
  text = null,
  children = null,
  root = null,
  listener = {},
}) => {
  const elem = document.createElement(name || 'div')

  if (className) {
    if (Array.isArray(className)) {
      elem.className = className.join(' ')
    }
    else {
      elem.className = className
    }
  }

  if (style) {
    let styleString = ''
    for (let prop of Object.getOwnPropertyNames(style)) {
      styleString += `${prop}: ${style[prop]};`
    }
    elem.setAttribute('style', styleString)
  }

  /** Это жесть с индексацией стрингами. Надо бы доработать, но я не знаю, откуда брать типы для атрибутов HTML элементов */
  if (attributes) {
    for (let prop of Object.getOwnPropertyNames(attributes)) {
      elem.setAttribute(prop, attributes[prop])
    }
  }

  if (text) elem.innerText = text

  if (listener) {
    elem.addEventListener(listener.eventName, listener.callback, listener.options)
  }

  if (children) {
    let resultChildren = []
    if ( !Array.isArray(children) ) resultChildren.push(children)
    else resultChildren = children

    for (let child of resultChildren) {
      elem.appendChild(child)
    }
  }

  if (root) root.appendChild(elem)

  return elem
}