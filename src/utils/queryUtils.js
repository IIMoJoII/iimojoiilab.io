export const getQueryParams = (hash) => {
  const params = hash.slice(1).split('&')

  return params.reduce((result, param) => {
    const [key, value] = param.split('=')
    return Object.assign(result, {
      [key]: value,
    })
  }, {})
}

export const constructQueryString = obj => Object.entries(obj).reduce((result, [key, value], index, array) => {
  if (index === 0) result += '?'
  if (value !== undefined) result += `${key}=${encodeURI(value)}`
  if (index !== array.length - 1) result += '&'
  return result
}, '')
