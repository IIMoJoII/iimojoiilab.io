import type { Component } from "./types/types";

import * as rollup from "rollup/dist/es/rollup.browser.js";

const CDN_URL = "https://cdn.jsdelivr.net/npm";
importScripts(`${CDN_URL}/svelte/compiler.js`);

const component_lookup: Map<string, Component> = new Map();

async function fetch_package(url: string): Promise<string> {
    return (await fetch(url)).text();
}

function generate_lookup(components: Component[]): void {
    components.forEach((component) => {
        component_lookup.set(`${component.name}.${component.type}`, component);
    });
}

self.addEventListener(
    "message",
    async (event: MessageEvent<Component[]>): Promise<void> => {
        generate_lookup(event.data);

        try {
            const bundle = await rollup.rollup({
                input: "App.svelte",
                plugins: [
                    {
                        name: "repl-plugin",
                        async resolveId(importee: string, importer: string) {
                            // handle imports from 'svelte'

                            if (importee === 'imask') {
                                return `${CDN_URL}/imask@6.4.2/esm/index.js`
                            }

                            if (importer && importer.startsWith(`${CDN_URL}/imask@6.4.2/esm`)) {
                                return new URL(importee, importer).href
                            }

                            // import x from 'svelte'
                            if (importee === "svelte") return `${CDN_URL}/svelte/index.mjs`;

                            // import x from 'svelte/somewhere'
                            if (importee.startsWith("svelte/")) {
                                return `${CDN_URL}/svelte/${importee.slice(7)}/index.mjs`;
                            }

                            // import x from './file.js' (via a 'svelte' or 'svelte/x' package)
                            if (importer && importer.startsWith(`${CDN_URL}/svelte`)) {
                                const resolved = new URL(importee, importer).href;
                                if (resolved.endsWith(".mjs")) return resolved;
                                return `${resolved}/index.mjs`;
                            }


                            if(importee.includes('../')) {
                                let split = importee.split('../').length
                                let importerPath = ''
                                let importeeName = importee.replace(/\.\.\//g, "")
                                let root = false

                                let substring = importer?.split('/')

                                for(let i = 0; i < split; i++) {
                                    i === 0 && (importerPath = importerPath + substring[i])
                                    i !== 0 && (importerPath = importerPath + "/" + substring[i])
                                    substring[i]?.includes('.') && (root = true)
                                }

                                importee = `${importerPath}/${importeeName}`

                                root && (importee = importeeName)
                            }

                            if(importee.includes('./')) {
                                let importerPath = importer?.substring(0, importer.lastIndexOf('/'))
                                let importeeName = importee.replace(/\.\//g, "")

                                if(importerPath) {
                                    importee = `${importerPath}/${importeeName}`
                                } else {
                                    importee = importeeName
                                }
                            }

                            // local repl components
                            if (component_lookup.has(importee)) return importee;
                        },
                        async load(id: string) {
                            // local repl components are stored in memory
                            // this is our virtual filesystem
                            if (component_lookup.has(id))
                                return component_lookup.get(id).source;

                            // everything else comes from a cdn
                            return await fetch_package(id);
                        },
                        transform(code: string, id: string) {
                            // our only transform is to compile svelte components
                            //@ts-ignore
                            if (/.*\.svelte/.test(id)) return svelte.compile(code).js.code;
                        },
                    },
                ],
            });

            // a touch longwinded but output contains an array of chunks
            // we are not code-splitting, so we only have a single chunk
            const output: string = (await bundle.generate({ format: "esm" })).output[0]
                .code;

            self.postMessage(output);
        } catch (e) {
            self.postMessage({
                type: 'error',
                message: e.message,
                fullMessage: e
            })
        }
    }
);