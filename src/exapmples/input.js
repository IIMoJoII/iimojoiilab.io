export let inputExample = [
    {
        id: 0,
        name: "App",
        type: "svelte",
        show: true,
        root: true,
        source: `<script>
    import Input from './components/Input.svelte'
    import { emailValidation, validateLetters } from './utils/validation.js'

    let
        form = {
            email: '',
            firstName: '',
            middleName: '',
            lastName: ''
        },
        errors = {
            email: '',
            firstName: '',
            middleName: '',
            lastName: ''
        }
</script>

<div class="wrapper">
    <Input 
        bind:value={form.email}
        label="Email"
        placeholder="example@mail.ru"
        maxlength={50}
        validator={(value) => {
            !emailValidation(value) ? 
                errors.email = 'Некорректный адреc почты' :
                errors.email = ''
        }}
        bind:error={errors.email}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.firstName}
        label="Имя"
        placeholder="Имя"
        maxlength={17}
        validator={(value) => {
            (value?.length && value?.length < 2) ? errors.firstName = 'Минимальная длина 2 символа' :
            value?.length > 16 ? errors.firstName = 'Максимальная длина 16 символов' :
            !validateLetters(value) ? errors.firstName = 'Разрешены только буквы' :
            errors.firstName = ''
        }}
        bind:error={errors.firstName}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.middleName}
        label="Отчество"
        maxlength={17}
        placeholder="Отчество"
        validator={(value) => {
            (value?.length && value?.length < 2) ? errors.middleName = 'Минимальная длина 2 символа' :
            value?.length > 16 ? errors.middleName = 'Максимальная длина 16 символов' :
            !validateLetters(value) ? errors.middleName = 'Разрешены только буквы' :
            errors.middleName = ''
        }}
        bind:error={errors.middleName}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.lastName}
        label="Фамилия"
        placeholder="Фамилия"
        validator={(value) => {
            (value?.length && value?.length < 2) ? errors.lastName = 'Минимальная длина 2 символа' :
            value?.length > 16 ? errors.lastName = 'Максимальная длина 16 символов' :
            !validateLetters(value) ? errors.lastName = 'Разрешены только буквы' :
            errors.lastName = ''
        }}
        bind:error={errors.lastName}
    />
    
    <div class="output">
        <span>email: {form.email}</span>
        <span>firstName: {form.firstName}</span>
        <span>middleName: {form.middleName}</span>
        <span>lastName: {form.lastName}</span>
    </div>

    <div class="output">
        <span>email: {errors.email}</span>
        <span>firstName: {errors.firstName}</span>
        <span>middleName: {errors.middleName}</span>
        <span>lastName: {errors.lastName}</span>
    </div>
</div>

<style>
    .wrapper {
        display: flex;
        flex-direction: column;
    }
    
    .output {
        margin-top: 40px;
        display: flex;
        flex-direction: column;
    }
    
    .spacer {
        margin-top: 12px
    }
</style>

`
    },
    {
        id: 1,
        name: "components/Input",
        type: "svelte",
        show: true,
        source: `<script>
    import { clickOutside } from '../utils/clickOutside.js';
    
    export let 
        input = null,
        options = null,
        value = '',
        label = '',
        placeholder = '',
        validator,
        error = '',
        maxlength = 100

    let focus = false
    
    const onInput = (e) => {
        value = e.target.value
        validator(e.target.value)
    }
</script>

<div class="wrapper">
    <input
        class="input" 
        class:inputFocus={focus}
        class:inputValFocus={focus && value}
        class:inputError={error}
        on:click={() => focus = true}
        bind:this={input}
        {maxlength}
        on:input={onInput}
        use:clickOutside
        on:click_outside={() => focus = false}
    />
    <div
        class="label"
        class:labelFocus={focus}
        class:labelValue={focus || value}
        on:click={() => {
            input?.focus()
            focus = true
        }}
    >
        {focus ? label : value ? label : placeholder}
    </div>
    <div class="error">
        {error ? error : ''}
    </div>
</div>


<style>
    .wrapper {
        position: relative;    
    }
    
    .input {
        box-sizing: border-box;
        outline: none;
        width: 300px;
        height: 45px;
        border: 1px solid gray;
        padding: 13px 15px 0px 15px;
        border-radius: 4px;
    }
    
    .inputFocus {
        border: 1px solid orange;
    }
    
    .inputError {
        border: 1px solid red;
    }
    
    .label {
        top: 0;
        left: 0;
        padding: 15px 15px 0px 15px;
        font-size: 14px;
        color: gray;
        position: absolute;
        transition: 0.3s;
    }
    
    .labelFocus {
        padding: 5px 15px 0px 15px;
    }
    
    .labelValue {
        padding: 5px 15px 0px 15px;
    }
    
    .error {
        margin-top: 3px;
        color: red;
    }
</style>`
    },
    {
        id: 2,
        name: "utils/clickOutside",
        type: "js",
        show: true,
        source: `export function clickOutside(node) {
  
  const handleClick = event => {
    if (node && !node.contains(event.target) && !event.defaultPrevented) {
      node.dispatchEvent(
        new CustomEvent('click_outside', node)
      )
    }
  }

  document.addEventListener('click', handleClick, true);
  
  return {
    destroy() {
      document.removeEventListener('click', handleClick, true);
    }
  }
}`
    },
    {
        id: 4,
        name: "utils/validation",
        type: "js",
        show: true,
        source: `export const emailValidation = (value) => {
    let regex = /^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$/g
    if(value) return regex.test(value)
    return true
}
    
export const validateLetters = (value) => {
    let regex = /[^A-Za-zА-Яа-яёЁ]+/g
    if(value) return !regex.test(value)
    return true
}`
    },
]