export let selectExample = [
    {
        id: 0,
        name: "App",
        type: "svelte",
        show: true,
        root: true,
        source: `<script>
    import Select from './components/Select.svelte'
    
    let value = null
    
    let countries = [
        { id: 0, name: "Москва" },
        { id: 1, name: "Краснодар" },
        { id: 2, name: "Барнаул" },
        { id: 3, name: "Красноярск" },
        { id: 4, name: "Новосибирск" },
        { id: 5, name: "Санкт-Петербург" },
        { id: 6, name: "Севастополь" },
        { id: 7, name: "Ялта" },
    ]
</script>

<div class="wrapper">
    <Select 
        placeholder="Countries"
        label="Countries"
        dataPoint="name"
        data={countries}
        clearValue={() => {
            value = null
        }}
        valueModifier={(item) => {
            value = item
            return item.name
        }} 
        searcher={(searchString) => {
            return countries.filter(country => country.name.toLowerCase().includes(searchString.toLowerCase()))
        }}
    />

    <div class="output">
        {#if value}
            <span>
                id: {value?.id}
                <br/>
                name: {value?.name}
            </span>
        {:else}
            <span>null</span>
        {/if}
    </div>
</div>

<style>
    .wrapper {
        display: flex;
        
    }
    
    .output {
        margin-left: 20px;
    }
</style>
`
    },
    {
        id: 1,
        name: "components/Select",
        type: "svelte",
        show: true,
        root: true,
        source: `<script>
    import { clickOutside } from '../utils/clickOutside.js';
    
    export let 
        data = [],
        searchData = [],
        searcher = null,
        input = null,
        options = null,
        label = '',
        placeholder = '',
        validator,
        maxlength = 100,
        dataPoint = '',
        valueModifier = null,
        clearValue = null,
        bufferValue = ''

    let
        focus = false,
        searchString = '',
        value = ''
        
    const chooseItem = (item) => {
        value = valueModifier(item)
        bufferValue = value
        focus = false
    }    
    
    const onInput = (e) => {
        searchString = e.target.value
        searchData = searcher(e.target.value)
        
        if(!searchString) {
            clearValue()
            value = ''
        }
    }
</script>

<div
    class="wrapper"
    use:clickOutside
    on:click_outside={() => {
        focus = false
        searchString && (input.value = bufferValue)
    }}
>
    <input
        class="input" 
        class:inputFocus={focus}
        class:inputValFocus={focus && value}
        on:click={() => focus = !focus}
        bind:this={input}
        {maxlength}
        {value}
        on:input={onInput}
        readonly={!searcher ? false : !focus}
    />
    <div
        class="label"
        class:labelFocus={focus}
        class:labelValue={focus || value}
        on:click={() => {
            input?.focus()
            focus = true
        }}
    >
        {focus ? label : value ? label : placeholder}
    </div>
    <ul class="data">
        {#if focus}
            {#each searchString ? searchData : data as item}
                <li on:click={() => chooseItem(item)} class="dataItem">{item[dataPoint]}</li>
            {/each}
        {/if}
    </ul>
</div>


<style>
    .wrapper {
        position: relative;   
        width: 300px;
    }
    
    .input {
        box-sizing: border-box;
        outline: none;
        width: 300px;
        height: 45px;
        border: 1px solid gray;
        padding: 13px 15px 0px 15px;
        border-radius: 4px;
    }
    
    .inputFocus {
        border: 1px solid orange;
    }
    
    .label {
        top: 0;
        left: 0;
        padding: 15px 15px 0px 15px;
        font-size: 14px;
        color: gray;
        position: absolute;
        transition: 0.3s;
    }
    
    .labelFocus {
        padding: 5px 15px 0px 15px;
    }
    
    .labelValue {
        padding: 5px 15px 0px 15px;
    }
    
    .error {
        margin-top: 3px;
        color: red;
    }
    
    
    .data {
        max-height: 200px;
        overflow-y: scroll;
        padding: 0;
        list-style-type: none;
        position: absolute;
        margin-top: 4px;
        width: 100%;
        box-shadow: 0px 0px 1px rgba(10, 31, 68, 0.16), 0px 4px 16px rgba(10, 31, 68, 0.12);
        border-radius: 4px;
    }
    
    .data::-webkit-scrollbar {
        width: 6px;
    }
 
    .data::-webkit-scrollbar-track {
        background: transparent;
    }
 
    .data::-webkit-scrollbar-thumb {
        background: orange;
    }
    
    .dataItem {
        cursor: pointer;
        padding: 13px 12px;
    }
    
    .dataItem:hover {
        background: #FAFAFA;
        border-radius: 4px;
    }
    
</style>`
    },
    {
        id: 2,
        name: "utils/clickOutside",
        type: "js",
        show: true,
        source: `export function clickOutside(node) {
  
  const handleClick = event => {
    if (node && !node.contains(event.target) && !event.defaultPrevented) {
      node.dispatchEvent(
        new CustomEvent('click_outside', node)
      )
    }
  }

  document.addEventListener('click', handleClick, true);
  
  return {
    destroy() {
      document.removeEventListener('click', handleClick, true);
    }
  }
}`
    },
]