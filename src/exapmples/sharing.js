export let sharingExample = [
    {
        id: 0,
        name: "App",
        type: "svelte",
        show: true,
        root: true,
        source: `<script>
    import Sharing from './components/Sharing.svelte';
<\/script>

<Sharing />
			`
    },
    {
        id: 1,
        name: "components/Sharing",
        type: "svelte",
        show: true,
        source:`<script>
    import { getSharingLinkVK, getSharingLinkFB, getSharingLinkOK } from 'utils/utils.js'
<\/script>

<div class="wrapper">
    <div class="sharing">
		<a
		  href={getSharingLinkVK({
			title: 'Русский тайтл',
			description: 'Русский дискрипшен',
			url: 'https://sobripie.gitlab.io',
			image: 'https://www.meme-arsenal.com/memes/83116ad7203ec2f076a1697e8ea6d98e.jpg'
		  })}
		  target="_blank"
		  rel="noreferrer"
		>
		  ВКОНТАКТЕ
		</a>

		<a
		  href={getSharingLinkFB({
    		url: 'https://sobripie.gitlab.io',
  		  })}
		  target="_blank"
		  rel="noreferrer"
		>
		  ФЕЙСБУК
		</a>

		<a
		  href={getSharingLinkOK({
    		title: 'Русский тайтл',
    		description: 'Русский дискрипшен',
    		url: 'https://sobripie.gitlab.io',
    		image: 'https://www.meme-arsenal.com/memes/83116ad7203ec2f076a1697e8ea6d98e.jpg'
          })}
		  target="_blank"
		  rel="noreferrer"
		>
		  ОДНОКЛАССНИКИ
		</a>
	</div>
</div>

<style>
	.wrapper {
		display: flex;
		flex-direction: column;
	}

	.sharing {
	   display: flex;
	   flex-direction: row;
	}

	a {
	   padding: 10px 20px;
	   border: 2px solid lightcoral;
	   margin-right: 20px;
	   text-decoration: none;
	   color: black;
	}
</style>
`
    },
    {
        id: 2,
        name: "utils/utils",
        type: "js",
        show: true,
        source: `import { constructQueryString } from 'utils/queryString.js'
        
export const getSharingLinkVK = ({url, title, description, image}) => 
 'http://vk.com/share.php' + constructQueryString({url, title, description, image})

export const getSharingLinkFB = ({url}) =>
 'http://www.facebook.com/sharer.php' + constructQueryString({u: url})

export const getSharingLinkOK = ({url, title, description, image}) =>
 'https://connect.ok.ru/offer' + constructQueryString({url, title, description, image})`
    },
    {
        id: 3,
        name: "utils/queryString",
        type: "js",
        show: true,
        source: `export const constructQueryString = obj => Object.entries(obj).reduce((result, [key, value], index, array) => {
  	if (index === 0) result += '?'
  	if (value !== undefined) result += key + '=' + encodeURI(value)
  	if (index !== array.length - 1) result += '&'

  	return result
}, '')`
    },
]