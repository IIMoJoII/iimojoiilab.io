export let maskedInputExample = [
    {
        id: 0,
        name: "App",
        type: "svelte",
        show: true,
        root: true,
        source: `<script>
    import MaskedInput from './components/MaskedInput.svelte'

    let
        form = {
            phone: '',
            code: ''
        },
        errors = {
            phone: '',
            code: ''
        }
</script>

<div class="wrapper">
    <MaskedInput
        bind:value={form.phone}
        label="Телефон"
        placeholder="+7 (___) ___-__-__"
        options={{
            mask: '+{7} (000) 000-00-00',
            prepare: (appended, masked) => {
                if (appended === '8' && masked.value === '') return ''
                return appended
            }
        }}
        bind:error={errors.phone}
    />
    <div class="spacer"></div>
    <MaskedInput 
        bind:value={form.code}
        label="Код"
        maxlength={17}
        placeholder="000000"
        options={{
            mask: '000000'
        }}
        bind:error={errors.code}
    />
    
    <div class="output">
        <span>phone: {form.phone}</span>
        <span>code: {form.code}</span>
    </div>

    <div class="output">
        <span>phone: {errors.phone}</span>
        <span>code: {errors.code}</span>
    </div>
</div>

<style>
    .wrapper {
        display: flex;
        flex-direction: column;
    }
    
    .output {
        margin-top: 40px;
        display: flex;
        flex-direction: column;
    }
    
    .spacer {
        margin-top: 12px
    }
</style>

`
    },
    {
        id: 2,
        name: "utils/clickOutside",
        type: "js",
        show: true,
        source: `export function clickOutside(node) {
  
  const handleClick = event => {
    if (node && !node.contains(event.target) && !event.defaultPrevented) {
      node.dispatchEvent(
        new CustomEvent('click_outside', node)
      )
    }
  }

  document.addEventListener('click', handleClick, true);
  
  return {
    destroy() {
      document.removeEventListener('click', handleClick, true);
    }
  }
}`
    },
    {
        id: 3,
        name: "utils/imask",
        type: "js",
        show: true,
        source: `import IMask from 'imask';


function fireEvent (el, eventName, data) {
  var e = document.createEvent('CustomEvent');
  e.initCustomEvent(eventName, true, true, data);
  el.dispatchEvent(e);
}

function reset () {
    if(maskRef) {
        maskRef.masked.reset();
    }
}

function initMask (el, opts) {
    
    
  const maskRef = (opts instanceof IMask.InputMask ? opts : IMask(el, opts));
  return maskRef
    .on('accept', () => fireEvent(el, 'accept', maskRef))
    .on('complete', () => fireEvent(el, 'complete', maskRef))
    .on('reset', () => fireEvent(el, 'reset', maskRef))
}


export default function imask(el, options) {
  let maskRef = options && initMask(el, options);

  function destroy () {
    if (maskRef) {
      maskRef.destroy();
      maskRef = undefined;
    }
  }

  function update (options) {
    if (options) {
      if (maskRef) {
        if (options instanceof IMask.InputMask) maskRef = options;
        else maskRef.updateOptions(options);
      }
      else maskRef = initMask(el, options);
    } else {
      destroy();
    }
  }

  return {
    update,
    destroy,
    reset
  };
}`
    },
    {
        id: 5,
        name: "components/MaskedInput",
        type: "svelte",
        show: true,
        source: `<script>
    import imask from '../utils/imask.js'
    import { clickOutside } from '../utils/clickOutside.js';
    
    export let 
        input = null,
        options = null,
        value = '',
        label = '',
        placeholder = '',
        validator,
        error = '',
        maxlength = 100

    let focus = false
    
    const accept = ({ detail: maskRef }) => {
        value = maskRef.unmaskedValue
        error = ''
    }
</script>

<div class="wrapper">
    <input
        class="input" 
        class:inputFocus={focus}
        class:inputValFocus={focus && value}
        class:inputError={error}
        on:click={() => focus = true}
        bind:this={input}
        use:imask={options}
        on:accept={accept}
        use:clickOutside
        on:click_outside={() => focus = false}
    />
    <div
        class="label"
        class:labelFocus={focus}
        class:labelValue={focus || value}
        on:click={() => {
            input?.focus()
            focus = true
        }}
    >
        {focus ? label : value ? label : placeholder}
    </div>
    <div class="error">
        {error ? error : ''}
    </div>
</div>


<style>
    .wrapper {
        position: relative;    
    }
    
    .input {
        box-sizing: border-box;
        outline: none;
        width: 300px;
        height: 45px;
        border: 1px solid gray;
        padding: 13px 15px 0px 15px;
        border-radius: 4px;
    }
    
    .inputFocus {
        border: 1px solid orange;
    }
    
    .inputError {
        border: 1px solid red;
    }
    
    .label {
        top: 0;
        left: 0;
        padding: 15px 15px 0px 15px;
        font-size: 14px;
        color: gray;
        position: absolute;
        transition: 0.3s;
    }
    
    .labelFocus {
        padding: 5px 15px 0px 15px;
    }
    
    .labelValue {
        padding: 5px 15px 0px 15px;
    }
    
    .error {
        margin-top: 3px;
        color: red;
    }
</style>`
    }
]