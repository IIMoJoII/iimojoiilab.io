export let donutExample = [
    {
       id: 0,
       name: "App",
       type: "svelte",
       show: true,
       root: true,
       source: `<script>
  import Donut from './components/Donut.svelte'
  import Dashboard from './components/Dashboard.svelte'
        
  let data = [
    { percent: 10,   label: 'Ноль',          color: 'blue'          },
    { percent: 15,   label: 'Целковый',      color: 'orange'        },
    { percent: 20,   label: 'Полушка',       color: 'yellow'        },
    { percent: 10,   label: 'Четвертушка',   color: 'red'           },
    { percent: 10,   label: 'Подувичок',     color: 'BlueViolet'    },
    { percent: 10,   label: 'Медичок',       color: 'Brown'         },
    { percent:  5,   label: 'Серебрячок',    color: 'Crimson'       },
    { percent:  3,   label: 'Золотничок',    color: 'DarkGoldenRod' },
    { percent:  7,   label: 'Девятичок',     color: 'DarkViolet'    },
    { percent: 10,   label: 'Десятичок',     color: 'Olive'         },
  ]
<\/script>

<div class="wrapper">
  <Donut {data} />
  <Dashboard {data} />
</div>

<style>
  .wrapper {
    display: flex;
    flex-direction: row;
    align-items: center;
  }
</style>
`
    },
    {
        id: 1,
        name: "components/Donut",
        type: "svelte",
        show: true,
        source:`<script>
  export let data = []
  
  let
    prevRotation = 0,
    values = []

  data.map(data => {
    values.push({
      percent: data.percent,
      rotation: prevRotation,
      color: data.color
    })
    
    prevRotation = prevRotation + data.percent
  })
    
<\/script>

<div class="wrapper">
  <svg class="chart" viewBox="0 0 40 40">
    <g 
        fill="none"
        class="group"
        stroke-width="3" 
        transform="rotate(-90)"
    >
      {#each values as data}
        <circle
            class="ring"
            stroke={data.color}
            stroke-dasharray={data.percent + ' ' + '100'}
            style={'--rotation: ' + data.rotation}
            cx="20"
            cy="20"
            r="15.91549430918954"
        ></circle>
      {/each}
    </g>
  </svg>
</div>


<style>
  .wrapper {
    height: 400px;
    display: grid;
    place-content: center;
  }

  .chart {
    max-width: 400px;
    width: 400px;
  }

  .group,
  .ring {
    transform-origin: 20px 20px;
  }

  .ring {
    transform: rotate(calc(360deg * var(--rotation, 0) / 100));
    transition: .333s stroke-width ease;
    cursor: pointer;
  }
  
  .ring:hover {
    stroke-width: 3.5;
  }

</style>
`
    },
    {
        id: 2,
        name: "components/Dashboard",
        type: "svelte",
        show: true,
        root: true,
        source: `<script> 
  export let data = []
<\/script>


<ul>
  {#each data as item}
    <li class='item'>
      <div class='point' style={'background: ' + item.color}></div>
      <span>{item.label} {item.percent}%</span>
    </li>
  {/each}
</ul>


<style>
  .item {
    list-style-type: none;
    margin-bottom: 10px;
    display: flex;
    flex-direction: row;
    align-items: center;
    font-size: 20px;
  }
  
  .point {
    width: 14px;
    height: 14px;
    border-radius: 50%;
    margin-right: 10px;
  }
</style>
`
    },
]