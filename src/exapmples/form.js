export let formExample = [
    {
        id: 0,
        name: "App",
        type: "svelte",
        show: true,
        root: true,
        source: ` <script>
    import Form from  './components/Form.svelte'
    import Input from './components/Input.svelte'
    import { emailValidation, validateLetters } from './utils/validation.js'
    
    // Тег Form создан для того, чтобы управлять поведением инпутов
    // При нажатии на Enter - форма выполнит функцию sendForm
    // Тег Form может содержать child
    
    let
        form = {
            phone: '',
            email: '',
            firstName: '',
            middleName: '',
            lastName: '',
            code: ''
        },
        errors = {
            phone: '',
            email: '',
            firstName: '',
            middleName: '',
            lastName: '',
            code: ''
        }
        
    const sendForm = () => {
        !form.email.length &&
            (errors.email = 'Это поле обязательно для заполнения')
            
        if(!form.phone.length)
            errors.phone = 'Это поле обязательно для заполнения'
        else if(form.phone && form.phone.length < 11) 
            errors.phone = 'Номер телефона должен содержать 11 цифр'
        else errors.phone = ''
        
        if(form.firstName && form.firstName.length < 2)
            errors.firstName = 'Длина имени должна быть больше 1го символа'
        else errors.firstName = ''
        
        if(form.middleName && form.middleName.length < 2)
            errors.middleName = 'Длина имени должна быть больше 1го символа'
        else errors.middleName = ''
        
        if(form.lastName && form.lastName.length < 2)
            errors.lastName = 'Длина имени должна быть больше 1го символа'
        else errors.lastName = ''
            
        if(form.code && form.code.length < 6)
            errors.code = 'Код должен содержать 6 цифр'
        else errors.code = ''
        
        
        // Проверяем, что errors не содержит ошибок
        let checkErrorsExists = false
        Object.keys(errors).map(key =>
            errors[key] && (checkErrorsExists = true)
        );
        
        !checkErrorsExists && alert('form sended')
    }    
</script>

<div class="wrapper">
    <Form
        sendForm={sendForm}
    >
        <Input
        bind:value={form.phone}
        label="Телефон"
        placeholder="+7 (___) ___-__-__"
        options={{
            mask: '+{7} (000) 000-00-00',
            prepare: (appended, masked) => {
                if (appended === '8' && masked.value === '') return ''
                return appended
            }
        }}
        bind:error={errors.phone}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.email}
        label="Email"
        placeholder="example@mail.ru"
        maxlength={50}
        validator={(value) => {
            !emailValidation(value) ? 
                errors.email = 'Некорректный адреc почты' :
                errors.email = ''
        }}
        bind:error={errors.email}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.firstName}
        label="Имя"
        placeholder="Имя"
        maxlength={17}
        validator={(value) => {
            value?.length > 16 ? errors.firstName = 'Максимальная длина 16 символов' :
            !validateLetters(value) ? errors.firstName = 'Разрешены только буквы' :
            errors.firstName = ''
        }}
        bind:error={errors.firstName}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.middleName}
        label="Отчество"
        maxlength={17}
        placeholder="Отчество"
        validator={(value) => {
            value?.length > 16 ? errors.middleName = 'Максимальная длина 16 символов' :
            !validateLetters(value) ? errors.middleName = 'Разрешены только буквы' :
            errors.middleName = ''
        }}
        bind:error={errors.middleName}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.lastName}
        label="Фамилия"
        placeholder="Фамилия"
        validator={(value) => {
            value?.length > 16 ? errors.lastName = 'Максимальная длина 16 символов' :
            !validateLetters(value) ? errors.lastName = 'Разрешены только буквы' :
            errors.lastName = ''
        }}
        bind:error={errors.lastName}
    />
    <div class="spacer"></div>
    <Input 
        bind:value={form.code}
        label="Код"
        maxlength={17}
        placeholder="000000"
        options={{
            mask: '000000'
        }}
        bind:error={errors.code}
    />
        <div class="spacer"></div>
        <button
            class="button"
            type="submit"
            on:click|preventDefault={sendForm}
        >Отправить
        </button>
    </Form>
    
    <div class="output">
        <span>{form.phone}</span>
        <span>{form.email}</span>
        <span>{form.firstName}</span>
        <span>{form.middleName}</span>
        <span>{form.lastName}</span>
        <span>{form.code}</span>
    </div>
    
    <div class="output">
        <span>{errors.phone}</span>
        <span>{errors.email}</span>
        <span>{errors.firstName}</span>
        <span>{errors.middleName}</span>
        <span>{errors.lastName}</span>
        <span>{errors.code}</span>
    </div>
</div>

<style>
    .wrapper {
        display: flex;
        flex-direction: row;
    }
    
    .spacer {
        margin-top: 20px
    }
    
    .button {
        cursor: pointer;
        color: white;
        width: 300px;
        height: 40px;
        font-size: 18px;
        outline: none;
        border: none;
        background: orange;
        border-radius: 4px;
    }
    
    .output {
         display: flex;
        flex-direction: column;
        margin-left: 40px;
    }
</style>

`
    },
    {
        id: 6,
        name: "components/Form",
        type: "svelte",
        show: true,
        source: `<script>
    export let sendForm
</script>

<form 
    on:keydown={(e) => {
        if (e.key === 'Enter') {
          e.preventDefault()
          sendForm()
        }
    }}
>
    <slot></slot>
</form>

<style>
</style>`
    },
    {
        id: 1,
        name: "components/Input",
        type: "svelte",
        show: true,
        source: `<script>
    import imask from '../utils/imask.js'
    import { clickOutside } from '../utils/clickOutside.js';
    
    export let 
        input = null,
        options = null,
        value = '',
        label = '',
        placeholder = '',
        validator,
        error = '',
        maxlength = 100

    let
        focus = false,
        timer
    
    const accept = ({ detail: maskRef }) => {
        value = maskRef.unmaskedValue
        error = ''
    }
    
    const onInput = (e) => {
        value = e.target.value
        validator(e.target.value)
    }
</script>

<div class="wrapper">
    {#if options}
        <input
            class="input" 
            class:inputFocus={focus}
            class:inputValFocus={focus && value}
            class:inputError={error}
            on:click={() => focus = true}
            bind:this={input}
            use:imask={options}
            on:accept={accept}
            use:clickOutside
            on:click_outside={() => focus = false}
        />
    {/if}
    {#if !options}
        <input
            class="input" 
            class:inputFocus={focus}
            class:inputValFocus={focus && value}
            class:inputError={error}
            on:click={() => focus = true}
            bind:this={input}
            {maxlength}
            on:input={onInput}
            use:clickOutside
            on:click_outside={() => focus = false}
        />
    {/if}
    <div
        class="label"
        class:labelFocus={focus}
        class:labelValue={focus || value}
        on:click={() => {
            input?.focus()
            focus = true
        }}
    >
        {focus ? label : value ? label : placeholder}
    </div>
    <div class="error">
        {error ? error : ''}
    </div>
</div>


<style>
    .wrapper {
        position: relative;    
    }
    
    .input {
        box-sizing: border-box;
        outline: none;
        width: 300px;
        height: 45px;
        border: 1px solid gray;
        padding: 13px 15px 0px 15px;
        border-radius: 4px;
    }
    
    .inputFocus {
        border: 1px solid orange;
    }
    
    .inputError {
        border: 1px solid red;
    }
    
    .label {
        top: 0;
        left: 0;
        padding: 15px 15px 0px 15px;
        font-size: 14px;
        color: gray;
        position: absolute;
        transition: 0.3s;
    }
    
    .labelFocus {
        padding: 5px 15px 0px 15px;
    }
    
    .labelValue {
        padding: 5px 15px 0px 15px;
    }
    
    .error {
        margin-top: 3px;
        color: red;
    }
</style>`
    },
    {
        id: 2,
        name: "utils/clickOutside",
        type: "js",
        show: true,
        source: `export function clickOutside(node) {
  
  const handleClick = event => {
    if (node && !node.contains(event.target) && !event.defaultPrevented) {
      node.dispatchEvent(
        new CustomEvent('click_outside', node)
      )
    }
  }

  document.addEventListener('click', handleClick, true);
  
  return {
    destroy() {
      document.removeEventListener('click', handleClick, true);
    }
  }
}`
    },
    {
        id: 3,
        name: "utils/imask",
        type: "js",
        show: true,
        source: `import IMask from 'imask';


function fireEvent (el, eventName, data) {
  var e = document.createEvent('CustomEvent');
  e.initCustomEvent(eventName, true, true, data);
  el.dispatchEvent(e);
}

function reset () {
    if(maskRef) {
        maskRef.masked.reset();
    }
}

function initMask (el, opts) {
    
    
  const maskRef = (opts instanceof IMask.InputMask ? opts : IMask(el, opts));
  return maskRef
    .on('accept', () => fireEvent(el, 'accept', maskRef))
    .on('complete', () => fireEvent(el, 'complete', maskRef))
    .on('reset', () => fireEvent(el, 'reset', maskRef))
}


export default function imask(el, options) {
  let maskRef = options && initMask(el, options);

  function destroy () {
    if (maskRef) {
      maskRef.destroy();
      maskRef = undefined;
    }
  }

  function update (options) {
    if (options) {
      if (maskRef) {
        if (options instanceof IMask.InputMask) maskRef = options;
        else maskRef.updateOptions(options);
      }
      else maskRef = initMask(el, options);
    } else {
      destroy();
    }
  }

  return {
    update,
    destroy,
    reset
  };
}`
    },
    {
        id: 4,
        name: "utils/validation",
        type: "js",
        show: true,
        source: `export const emailValidation = (value) => {
    let regex = /^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$/g
    if(value) return regex.test(value)
    return true
}
    
export const validateLetters = (value) => {
    let regex = /[^A-Za-zА-Яа-яёЁ]+/g
    if(value) return !regex.test(value)
    return true
}`
    }
]