export interface Tab {
    id: number
    name: string
    type: string
    root?: boolean
    path?: Array<string>
}

export interface Component extends Tab {
    source: string,
    show: boolean,
}

export interface File {
    name: string,
    path: string,
    type: string,
    parent: string
}